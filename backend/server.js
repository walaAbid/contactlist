const express = require("express");
const mongoose = require('mongoose') 
const bodyParser = require("body-parser");
const assert = require('assert');
const MongoURL = "mongodb://localhost:27017";
const database = "ContactList";

const app = express();
app.use(bodyParser.json());

const Contact = require('./models/Contact')

// MongoClient.connect(MongoURL, { useNewUrlParser: true }, (err, client) => {
//   assert.equal(err, null, "database connection failed");
//   const db = client.db(database);

mongoose.connect('mongodb://localhost:27017/ContactList')
  .then(connect => console.log('database connected'))
  .catch(err => console.log(err))

  app.get("/contacts", (req, res) => {
    // db.collection("contacts")
    //   .find()
    //   .toArray()
    Contact.find()
      .then(contacts => {
        res.json(contacts);
      })
      .catch(err => err.status(404).json({ nopostsfound: "no contact found" }));
  });

  app.post('/addContact',(req,res)=>{
      let newContact = req.body;
      // db.collection("contacts")
      // .insertOne(newContact,(err,data)=>{
      //     if(err) res.send('connot add new contact');
      //     else res.send("new contact added");
      // })
      Contact(newContact)
        .save()
        .then(newContact => res.json(newContact))
        .catch(err => console.log(err));

  })
  app.put('/modifyContact/:id',(req,res)=>{
      // let contactID=ObjectID(req.params.id);
      // let updateContact=req.body;

      // db.collection("contacts").findOneAndUpdate(
      //     {_id:contactID},
      //     {$set:{...updateContact}},
      //     (err,data)=>{
      //         if(err)res.send(err);
      //         else res.send(data)
      //     }
      // );
      Contact.findOneAndUpdate(
        { _id: req.params.id },
        { $set: { ...req.body }},
        { new: true }
      )
        .then(updated => res.json(updated))
        .catch(err => console.log(err));
  })
  app.delete('/deleteContact/:id',(req,res)=>{
    // let contactID=ObjectID(req.params.id);
    // db.collection("contacts")
    // .deleteOne({_id:contactID})
    Contact.findOneAndDelete({ _id: req.params.id})
    .then(contacts=>res.json(contacts))
    .catch(err => err.status(404).json({ nopostsfound: "cannot delet contact" }))

  })
// });

app.listen(6000, err => {
  if (err) {
    console.log("server is not running ");
  } else console.log("server is running an port 6000");
});

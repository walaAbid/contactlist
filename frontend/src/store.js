import {createStore,combineReducers,applyMiddleware,compose} from 'redux'
import contactsReducer from './reducers/contacts-reducer'
import handleChange from './reducers/handle-change-reducer'
import thunk from 'redux-thunk';
const middleware=[thunk];

 const store = createStore(combineReducers({
    contactsReducer,
    handleChange
}),compose(applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))
export default store
import {GET_CONTACTS,ADD_CONTACTS,DELETE_CONTACTS,EDIT_CONTACTS} from '../actions/actionType'

const initialState = [
    // { name: "mourad", phone: 122165, email: "mourad@gmail.com" },
    // { name: "sarra", phone: 122165, email: "sarra@gmail.com" },
    // { name: "wala", phone: 122165, email: "wala@gmail.com" }
];
const contactsReducer=(state=initialState,action)=>{
    switch(action.type){
        case GET_CONTACTS:
            return action.payload

        case ADD_CONTACTS:
            return state

        case DELETE_CONTACTS:
            // return state.filter((el,index)=>index !== action.payload)
            return state
        case EDIT_CONTACTS:
            return state
    }
    return state

}
export default contactsReducer
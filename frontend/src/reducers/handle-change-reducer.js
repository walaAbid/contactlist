import { GET_NAME, GET_PHONE, GET_EMAIL, CLEAR_INPUTS } from "../actions/actionType";
const initilState = {name:"",phone:"",email:""};
const handleChange = (state = initilState, action) => {
  switch (action.type) {
    case GET_NAME:
        state.name= action.payload
      return state;
    case GET_PHONE:
            state.phone= action.payload
            return state;
    case GET_EMAIL:
            state.email= action.payload
            return state;
    case CLEAR_INPUTS:
        state = {name:"",phone:"",email:""}
            return state;
    default:
      return state;
  }
};
export default handleChange;

import React, { Component } from "react";
import "./App.css";
import Main from "./component/Main";
import ListContact from "./component/ListContact";
import AddContact from "./component/AddContact";
import { Route } from "react-router-dom";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      name: "",
      email: "",
      edit: false,
      id: ""
    };
  }

  handleInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  clear = () => {
    this.setState({
      phone: "",
      name: "",
      email: "",
      id: ""
    });
  };
  getPerson = (name, phone, email) => {
    this.setState({
      name,
      email,
      phone
    });
  };
  onEdit = (edit, id) => {
    this.setState({
      edit,
      id
    });
  };
  render() {
    const { name, phone, email, edit, id } = this.state;
    return (
      <div className="App">
        <h1 className="parag_contact">Contact List</h1>
        <Main onEdit={this.onEdit} clear={this.clear} />

        <Route 
          path="/listContact"
          render={() => (
            <ListContact onEdit={this.onEdit} getPerson={this.getPerson} />
          )}
        />
        <Route
          path="/(addContact|editContact)/"
          render={() => (
            <AddContact
              clear={this.clear}
              handleInput={this.handleInput}
              name={name}
              email={email}
              phone={phone}
              edit={edit}
              id={id}
            />
          )}
        />
      </div>
    );
  }
}

import React, { Component } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import Icon from "@material-ui/core/Icon";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import { connect } from "react-redux";
import { deleteContact, getContact } from "../actions/action";
import {Link} from 'react-router-dom';



const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fab: {
      margin: theme.spacing(1)
    },
    margin: {
      margin: theme.spacing(1)
    },
    extendedIcon: {
      marginRight: theme.spacing(1)
    }
  })
);

class ListContact extends Component {

  componentDidMount = () => {
    this.props.getContact();
  };
  render() {
    const { contacts, deleteContact, onEdit, getPerson} = this.props;
    return (
      <div className="contact_list">
        {contacts.map((el, index) => (
          <div className="list" key={index}>
            <h2>Contact</h2>
            <h4>Name: {el.name}</h4>
            <h4>Phone: {el.phone}</h4>
            <h4>Email: {el.email}</h4>
            <Link to="/editContact" className="button edit">
              <Fab
                color="secondary"
                aria-label="Edit"
                className={useStyles.fab}
                onClick={()=>{
                  onEdit(true, el._id);
                  getPerson(el.name,el.phone, el.email)
                }}
              >
                <Icon>edit_icon</Icon>
              </Fab>
         </Link>
            <span className="button delete">
              <IconButton
                aria-label="Delete"
                className={useStyles.margin}
                onClick={() => {
                  deleteContact(el._id);
                }}
              >
                <DeleteIcon fontSize="large" />
              </IconButton>
            </span>
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { contacts: state.contactsReducer };
};
const mapDispatchToProps = dispatch => {
  return {
    deleteContact: id => dispatch(deleteContact(id)),
    // editContact:( id,modifContact) => dispatch(editContact(id,modifContact)),
    getContact: () => dispatch(getContact())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListContact);

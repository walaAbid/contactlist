import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import {
  addContact,
  editContact,
  getName,
  getPhone,
  getEmail,
  clearInputs
} from "../actions/action";
import {Redirect} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  button: {
    margin: theme.spacing(1)
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  rightIcon: {
    marginLeft: theme.spacing(1)
  },
  dense: {
    marginTop: 19
  }
}));

class AddContact extends Component {
state={redirect:false}

  render() {
    const {
      addContact,
      editContact,
      edit,
      name,
      phone,
      email,
      clear,
      handleInput,
      id,
      getName,
      getPhone,
      getEmail,
      newContact,
      clearInputs
    } = this.props;
    
    return this.state.redirect ? <Redirect to ='/listContact'/>:(
      <div className="addContact">
       <p>{edit ? "Edit Contact": "Add Contact"}</p>
        <form>
          <input
            placeholder="Name"
            name="name"
            onChange={e => {
              handleInput(e);
            }}
            value={name}
          />
          <input
            placeholder="phone Number"
            name="phone"
            onChange={e => {
              handleInput(e);
            }}
            value={phone}
          />
          <input
            placeholder="email"
            name="email"
            onChange={e => {
              handleInput(e);
            }}
            value={email}
          />

          <Button
            variant="contained"
            color="primary"
            className={useStyles.button}
            onClick={() => {
              edit? editContact(id, {name,email,phone}) : addContact({name,phone,email});
              clear();
              this.setState({redirect:true});
              
            }}
          >
            {edit ? "Edit":"Send"}
            {/* This Button uses a Font Icon, see the installation instructions in the docs. */}
            <Icon className={useStyles.rightIcon}>send</Icon>
          </Button>
        </form>
        
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  addContact: payload => dispatch(addContact(payload)),
  editContact: (id, payload) => dispatch(editContact(id, payload)),
  // getName: payload => dispatch(getName(payload)),
  // getPhone: payload => dispatch(getPhone(payload)),
  // getEmail: payload => dispatch(getEmail(payload)),
  // clearInputs: () => dispatch(clearInputs())
});
// const mapStateToProps = state => {
//   return { newContact: state.handleChange };
// };
export default connect(
  null,
  mapDispatchToProps
)(AddContact);

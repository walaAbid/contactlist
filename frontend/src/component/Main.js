import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {Link} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
}));
export default function Main({ onEdit, clear }) {
  return (
    <div className="main_button">
      <span className="button primary">
        <Link to='/listContact'>
        <Button
          variant="contained"
          color="primary"
          className={useStyles.button}
        >
          List Contact
        </Button></Link>
      </span>
      <span className="button secondary">
     <Link to='/addContact'>
        <Button
          onClick={() => {
            onEdit(false);
            clear();
          }}
          variant="contained"
          color="secondary"
          className={useStyles.button}
        >
          Add Contact
        </Button>
        </Link>
      </span>
    </div>
  );
}

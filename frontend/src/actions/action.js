import axios from "axios";

import {
  GET_CONTACTS,
  ADD_CONTACTS,
  GET_NAME,
  GET_PHONE,
  GET_EMAIL,
  DELETE_CONTACTS,
  CLEAR_INPUTS,
  EDIT_CONTACTS
} from "./actionType";

// export const getContact=(payload)=>{
//     return {
//         type:GET_CONTACTS,
//         payload
//     }
// }
export const clearInputs = () => {
  return {
    type: CLEAR_INPUTS
  };
};

export const getContact = () => dispatch => {
  axios
    .get("/contacts")
    .then(res => {
      dispatch({
        type: GET_CONTACTS,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
    });
};

// export const addContact = payload => {
//   return {
//     type: ADD_CONTACTS,
//     payload
//   };
// };
export const addContact = newContact => dispatch => {
  axios
    .post("/addContact", newContact)
    .then(res => {
      dispatch({
        type: ADD_CONTACTS
      });
    })
    .then(() => dispatch(getContact()))
    .catch(err => {
      console.log(err);
    });
};

export const getName = payload => {
  return {
    type: GET_NAME,
    payload
  };
};
export const getPhone = payload => {
  return {
    type: GET_PHONE,
    payload
  };
};
export const getEmail = payload => {
  return {
    type: GET_EMAIL,
    payload
  };
};
// export const deleteContact = payload => {
//   return {
//     type: DELETE_CONTACT,
//     payload
//   };
// };
// export const deleteContact = id => dispatch=> {
//   axios.delete("/deleteContact/"+id)
//   .then(res => {
//     dispatch({
//       type: DELETE_CONTACTS
//     });
//   })
//   .then(() => dispatch(getContact()))
//   .catch(err => {
//     console.log(err);
//   });
// };
export const deleteContact = id => dispatch => {
  axios
    .delete("/deleteContact/" + id)
    .then(() => dispatch(getContact()))
    .then(() => {
      dispatch({
        type: DELETE_CONTACTS
      });
    })
    .catch(err => {
      console.log(err);
    });
};
export const editContact = (id,modifContact) => dispatch => {
  axios
    .put("/modifyContact/" + id, modifContact)
    .then(() => {
      dispatch({
        type: EDIT_CONTACTS
      });
    })
    .then(() => dispatch(getContact()))
    .catch(err => {
      console.log(err);
    });
};
